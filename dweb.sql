-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-12-2015 a las 18:22:49
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dweb`
--
CREATE DATABASE IF NOT EXISTS `dweb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `dweb`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `edad` int(2) NOT NULL,
  `fechaMatricula` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=104 ;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `nombre`, `apellidos`, `edad`, `fechaMatricula`) VALUES
(1, 'Laura', 'Gayan Baro', 29, '0000-00-00'),
(2, 'Ruben', 'Rodriguez', 35, '0000-00-00'),
(3, 'Rosa', 'Garcia', 22, '0000-00-00'),
(28, 'Carlos', 'Pueyo Cano', 27, '0000-00-00'),
(29, 'Lola', 'Moreno', 42, '0000-00-00'),
(30, 'Mario', 'Luna', 12, '0000-00-00'),
(36, 'Lila', 'Rosan', 45, '0000-00-00'),
(37, 'Hola', 'Caracola', 27, '0000-00-00'),
(39, 'Laura', 'Baro Gayan', 29, '0000-00-00'),
(100, 'Pepe', 'GarcÃ­a', 24, '0000-00-00'),
(101, 'Pepa', 'Gracia', 22, '0000-00-00'),
(102, 'Pepe', 'GarcÃ­a', 24, '0000-00-00'),
(103, 'Pepa', 'Gracia', 22, '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

CREATE TABLE IF NOT EXISTS `asignatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `nombreCorto` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nombreCompleto` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `asignatura`
--

INSERT INTO `asignatura` (`id`, `codigo`, `nombreCorto`, `nombreCompleto`) VALUES
(1, 'DES423', 'Entorno Servidor', 'Desarrollo en entorno servidor'),
(2, 'DEC632', 'Entorno Cliente', 'Desarrollo en entorno cliente');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
